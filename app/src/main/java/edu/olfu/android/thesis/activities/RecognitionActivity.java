package edu.olfu.android.thesis.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ipaulpro.afilechooser.utils.FileUtils;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfFloat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.ml.CvSVM;
import org.opencv.objdetect.HOGDescriptor;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.Bind;
import edu.olfu.android.thesis.R;
import edu.olfu.android.thesis.adapters.RVSimilarFlowerAdapter;
import edu.olfu.android.thesis.constants.Flower;
import edu.olfu.android.thesis.utils.UiUtil;

/**
 * Created by johneris on 2/2/16.
 */
public class RecognitionActivity extends BaseActivity {

    private static String TAG = "RecognitionActivity";

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, RecognitionActivity.class);
        return intent;
    }

    private static Mat sMat = null;
    private static Bitmap sBitmapResult = null;
    private static Bitmap sBitmapShape = null;
    private static Bitmap sBitmapHue = null;
    private static Bitmap sBitmapSaturation = null;
    private static Bitmap sBitmapValue = null;

    public static void setMat(Mat mat) {
        sMat = mat;
    }

    public static void setBitmapResult(Bitmap bitmapResult) {
        sBitmapResult = bitmapResult;
    }

    public static void setBitmapShape(Bitmap bitmapShape) {
        sBitmapShape = bitmapShape;
    }

    public static void setBitmapHue(Bitmap bitmapHue) {
        sBitmapHue = bitmapHue;
    }

    public static void setBitmapSaturation(Bitmap bitmapSaturation) {
        sBitmapSaturation = bitmapSaturation;
    }

    public static void setBitmapValue(Bitmap bitmapValue) {
        sBitmapValue = bitmapValue;
    }

    private static final int FILE_SELECT_CODE = 0;

    @Bind(R.id.ivResult)
    ImageView mIvResult;

    @Bind(R.id.tvFlowerName)
    TextView mTvFlowerName;

    @Bind(R.id.ivShape)
    ImageView mIvShape;

    @Bind(R.id.ivHue)
    ImageView mIvHue;

    @Bind(R.id.ivSaturation)
    ImageView mIvSaturation;

    @Bind(R.id.ivValue)
    ImageView mIvValue;

    @Bind(R.id.tvSimilarFlowers)
    TextView mTvSimilarFlowers;

    @Bind(R.id.rvSimilarFlowers)
    RecyclerView mRvSimilarFlowers;


    private static CvSVM sCvSVM;

    private Flower mFlower;
    private boolean mIsRecognitionInProcess;
    private boolean mIsRecognitionDone;

    private ProgressDialog mProgressDialog;


    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_recognition;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mIsRecognitionDone = false;
        mIsRecognitionInProcess = false;
        initUI();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!mIsRecognitionInProcess && !mIsRecognitionDone) {
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_11, this, mLoaderCallback);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case FILE_SELECT_CODE:
                if (resultCode == RESULT_OK) {

                    final Uri uri = data.getData();

                    // Get the File path from the Uri
                    String path = FileUtils.getPath(this, uri);

                    // Alternatively, use FileUtils.getFile(Context, Uri)
                    if (path != null && FileUtils.isLocal(path)) {
                        File file = new File(path);
                        initSVM(file);
                    } else {
                        Toast.makeText(mContext, "Failed to load file.",
                                Toast.LENGTH_SHORT).show();
                        finish();
                    }
                } else {
                    Toast.makeText(mContext, "Trained data for Flower Recognition is required.",
                            Toast.LENGTH_SHORT).show();
                    showFileChooser();
                }
                break;
        }
    }

    private void initUI() {
        setUpToolbar();
        mProgressDialog = UiUtil.getProgressDialog(mContext, "Please wait...");
        mIvResult.setImageBitmap(sBitmapResult);
    }

    private void setUpToolbar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle("Recognition");
            actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    private void showFileChooser() {
        Intent getContentIntent = FileUtils.createGetContentIntent();

        Intent intent = Intent.createChooser(getContentIntent, "Select trained data for Flower Recognition");
        startActivityForResult(intent, FILE_SELECT_CODE);
    }

    private void onOpenCVLoaded() {
        if (sCvSVM == null) {
            sCvSVM = new CvSVM();
            showFileChooser();
        } else {
            onSVMInitialized();
        }
    }

    private void initSVM(File file) {
        if (!file.exists()) {
            UiUtil.showMessageDialog(getSupportFragmentManager(), "Trained data missing!\n\n" +
                    file.getAbsolutePath());
            showFileChooser();
            return;
        }

        mIsRecognitionDone = false;
        mIsRecognitionInProcess = true;

        sCvSVM = new CvSVM();
        new InitSVMAsyncTask().execute(file.getAbsolutePath());
    }

    private void onSVMInitialized() {
        new RecognitionAsyncTask().execute();
    }

    private void onRecognitionEnded() {
        mIsRecognitionInProcess = false;
        mIsRecognitionDone = true;

        if (mFlower == Flower.aster) {
            mFlower = Flower.negative;
        }

        setResult(mFlower.getName());

        if (mFlower == Flower.negative) {
            mTvSimilarFlowers.setVisibility(View.GONE);
            mRvSimilarFlowers.setVisibility(View.GONE);
            return;
        }

        mTvFlowerName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = FlowerDetailsActivity.newIntent(mContext, mFlower.ordinal());
                mContext.startActivity(intent);
            }
        });

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mRvSimilarFlowers.setLayoutManager(linearLayoutManager);
        mRvSimilarFlowers.setHasFixedSize(true);

        List<Flower> flowerList = new ArrayList<>();
        List<Integer> imageResList = new ArrayList<>();

        Flower[] similarFlowers = mFlower.similarFlowers();
        for (Flower flower : similarFlowers) {
            for (int imgRes : flower.getUniquFlowerImageRes()) {
                flowerList.add(flower);
                imageResList.add(imgRes);
            }
        }

        for (int imgRes : mFlower.getUniquFlowerImageRes()) {
            flowerList.add(mFlower);
            imageResList.add(imgRes);
        }

        RVSimilarFlowerAdapter rvSimilarFlowerAdapter = new RVSimilarFlowerAdapter(mContext, flowerList, imageResList);
        mRvSimilarFlowers.setAdapter(rvSimilarFlowerAdapter);
    }

    private void setResult(String flowerName) {
        mIvResult.setImageBitmap(sBitmapResult);
        mTvFlowerName.setText(flowerName);
        mIvShape.setImageBitmap(sBitmapShape);
        mIvHue.setImageBitmap(sBitmapHue);
        mIvSaturation.setImageBitmap(sBitmapSaturation);
        mIvValue.setImageBitmap(sBitmapValue);
    }

    private static Mat extractFeatures(Mat mat) {
        Mat featureMat = new Mat();
        HOGDescriptor hogDescriptor = new HOGDescriptor();

        // Canny Edge detection
        Mat cannyMat = new Mat();
        Imgproc.resize(mat, cannyMat, new Size(130, 130));
        Imgproc.Canny(mat, cannyMat, 80, 90);

        sBitmapShape = Bitmap.createBitmap(cannyMat.width(), cannyMat.height(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(cannyMat, sBitmapShape);

        // Get hog descriptor from canny edge
        MatOfFloat cannyDescriptor = new MatOfFloat();
        hogDescriptor.compute(cannyMat, cannyDescriptor);

        Mat cannyFeature = new Mat();
        // push 1D feature from canny edge detection + hog descriptor
        cannyFeature = cannyDescriptor.reshape(1, 1);

        // HSV feature

        Mat histMat = new Mat();
        Imgproc.resize(mat, histMat, new Size(150, 150));

        Imgproc.cvtColor(histMat, histMat, Imgproc.COLOR_BGR2HSV);

        // Get HSV channels separately
        List<Mat> listMatRGB = new ArrayList<>();
        Core.split(histMat, listMatRGB);

        Mat matR = listMatRGB.get(0);
        Mat matG = listMatRGB.get(1);
        Mat matB = listMatRGB.get(2);

        sBitmapHue = Bitmap.createBitmap(matR.width(), matR.height(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(matR, sBitmapHue);
        sBitmapSaturation = Bitmap.createBitmap(matG.width(), matG.height(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(matG, sBitmapSaturation);
        sBitmapValue = Bitmap.createBitmap(matB.width(), matB.height(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(matB, sBitmapValue);

        MatOfFloat descriptorR = new MatOfFloat();
        MatOfFloat descriptorG = new MatOfFloat();
        MatOfFloat descriptorB = new MatOfFloat();

        hogDescriptor.compute(matR, descriptorR);
        hogDescriptor.compute(matG, descriptorG);
        hogDescriptor.compute(matB, descriptorB);

        Mat rgbFeature = new Mat();

        Mat rev = descriptorR.reshape(1, 1);
        rgbFeature.push_back(rev);

        rev = descriptorG.reshape(1, 1);
        rgbFeature.push_back(rev);

        rev = descriptorB.reshape(1, 1);
        rgbFeature.push_back(rev);

        rgbFeature = rgbFeature.reshape(1, 1);

        Core.hconcat(Arrays.asList(cannyFeature, rgbFeature), featureMat);

        return featureMat;
    }

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS: {
                    Log.i(TAG, "OpenCV loaded successfully");
                    onOpenCVLoaded();
                }
                break;
                default: {
                    super.onManagerConnected(status);
                }
                break;
            }
        }
    };

    private class InitSVMAsyncTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            String fileName = params[0];
            sCvSVM.load(fileName);
            return "";
        }

        @Override
        protected void onPostExecute(String result) {
            mProgressDialog.hide();
            onSVMInitialized();
        }

        @Override
        protected void onPreExecute() {
            mProgressDialog.setMessage("Initializing SVM");
            mProgressDialog.show();
        }

    }

    private class RecognitionAsyncTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            Mat mat = new Mat();
            Imgproc.cvtColor(sMat, mat, Imgproc.COLOR_RGB2BGR);
            Mat features = extractFeatures(mat);
            int result = (int) sCvSVM.predict(features);
            for (Flower flower : Flower.values()) {
                if (flower.getLabel() == result) {
                    mFlower = flower;
                    break;
                }
            }
            Log.d(TAG, "RESULT:" + mFlower.getName());
            return "";
        }

        @Override
        protected void onPostExecute(String result) {
            mProgressDialog.hide();
            onRecognitionEnded();
        }

        @Override
        protected void onPreExecute() {
            mProgressDialog.setMessage("Recognition started");
            mProgressDialog.show();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return false;
    }

}
