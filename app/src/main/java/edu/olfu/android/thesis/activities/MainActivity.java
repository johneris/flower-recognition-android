package edu.olfu.android.thesis.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import org.joda.time.DateTime;
import org.joda.time.Days;

import java.util.Calendar;

import edu.olfu.android.thesis.R;

/**
 * Created by johneris on 6/1/2015.
 */
public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Calendar calendar = Calendar.getInstance();

        DateTime now = new DateTime(calendar.getTime());

        Calendar calendar2 = Calendar.getInstance();
        calendar2.set(2016, 1, 22);
        DateTime deadLine = new DateTime(calendar2.getTime());

        int daysDifference = daysDifference(now, deadLine);

        Log.d("Main", "now: " + now.toString());
        Log.d("Main", "deadLine: " + deadLine.toString());
        Log.d("Main", "days diff: " + daysDifference);
        if (daysDifference <= 0) {
            finish();
            return;
        }

        Intent intent = HomeActivity.newIntent(mContext);
        startActivity(intent);
        finish();
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_main;
    }

    public int daysDifference(DateTime date1, DateTime date2) {
        return Days.daysBetween(date1, date2).getDays();
    }

}
