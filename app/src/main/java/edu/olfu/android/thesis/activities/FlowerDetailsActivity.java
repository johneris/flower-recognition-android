package edu.olfu.android.thesis.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.Bind;
import edu.olfu.android.thesis.R;
import edu.olfu.android.thesis.constants.Flower;

/**
 * Created by johneris on 2/2/16.
 */
public class FlowerDetailsActivity extends BaseActivity {

    private static final String ARGS_FLOWER = "ARGS_FLOWER";

    public static Intent newIntent(Context context, int flowerOrdinalInEnum) {
        Intent intent = new Intent(context, FlowerDetailsActivity.class);
        intent.putExtra(ARGS_FLOWER, flowerOrdinalInEnum);
        return intent;
    }

    @Bind(R.id.ivFlowerMain)
    ImageView mIvFlowerMain;

    @Bind(R.id.tvFlowerName)
    TextView mTvFlowerName;

    @Bind(R.id.ivFlower1)
    ImageView mIvFlower1;

    @Bind(R.id.ivFlower2)
    ImageView mIvFlower2;

    @Bind(R.id.ivFlower3)
    ImageView mIvFlower3;

    @Bind(R.id.tvDescription)
    TextView mTvDescription;

    private Flower mFlower;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_flower_details;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        int flowerOrdinal = getIntent().getIntExtra(ARGS_FLOWER, 0);

        Flower[] flowers = Flower.values();
        mFlower = flowers[flowerOrdinal];

        initUI();
    }

    private void initUI() {
        setUpToolbar();
        setFlowerDetails();
    }

    private void setFlowerDetails() {
        mIvFlowerMain.setImageResource(mFlower.getFlowerImage1());
        mTvFlowerName.setText(mFlower.getName());
        mIvFlower1.setImageResource(mFlower.getFlowerImage2());
        mIvFlower2.setImageResource(mFlower.getFlowerImage3());
        mIvFlower3.setImageResource(mFlower.getFlowerImage4());
        mTvDescription.setText(mFlower.getDescription());
    }

    private void setUpToolbar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle("");
            actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return false;
    }

}
