package edu.olfu.android.thesis.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import butterknife.Bind;
import butterknife.OnClick;
import edu.olfu.android.thesis.R;

/**
 * Created by johneris on 2/1/16.
 */
public class HomeActivity extends BaseActivity {

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, HomeActivity.class);
        return intent;
    }

    @Bind(R.id.btnRecognize)
    Button mBtnRecognize;

    @Bind(R.id.btnGallery)
    Button mBtnGallery;

    @Bind(R.id.btnAbout)
    Button mBtnAbout;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_home;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @OnClick(R.id.btnRecognize)
    void onButtonRecognizeClicked() {
        Intent intent = CameraActivity.newIntet(mContext);
        startActivity(intent);
    }

    @OnClick(R.id.btnGallery)
    void onButtonGalleryClicked() {
        Intent intent = GalleryActivity.newIntent(mContext);
        startActivity(intent);
    }

    @OnClick(R.id.btnAbout)
    void onButtonAboutClicked() {
        Intent intent = AboutActivity.newIntent(mContext);
        startActivity(intent);
    }


}
