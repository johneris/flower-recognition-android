package edu.olfu.android.thesis.constants;

import android.support.annotation.DrawableRes;

import edu.olfu.android.thesis.R;

public enum Flower {

    crysanthemum,
    gerbera,
    button_pom,
    carnation,
    malaysian_mums,
    rose,
    aster,
    negative;

    public int getLabel() {
        switch (this) {
            case crysanthemum:
                return 1;
            case gerbera:
                return 2;
            case button_pom:
                return 3;
            case carnation:
                return 4;
            case malaysian_mums:
                return 5;
            case rose:
                return 6;
            case aster:
                return 7;
            case negative:
                return 8;
            default:
                return -1;
        }
    }

    public String getName() {
        switch (this) {
            case crysanthemum:
                return "Crysanthemum";
            case gerbera:
                return "Gerbera";
            case button_pom:
                return "Button Pom";
            case carnation:
                return "Carnation";
            case malaysian_mums:
                return "Malaysian Mums";
            case rose:
                return "Rose";
            case aster:
                return "Aster";
            case negative:
                return "No Matching Flower Detected";
            default:
                return "";
        }
    }

    public String getDescription() {
        switch (this) {
            case crysanthemum:
                return "In the fall garden, chrysanthemums are the showstoppers, blooming prolifically well after other garden plants have called it quits for the season. Native to China and prized for over 2,000 years, the name \"chrysanthemum\" comes from the Greek words for gold (chrysos) and flower (anthos) and is often affectionately shortened to \"mum.\"";
            case gerbera:
                return "Distinguished by large flowering heads that closely resemble those of sunflowers, Gerbera daisies come in a vibrant rainbow of colors. The Gerbera daisy was discovered in 1884 near Barberton, South Africa, by Scotsman Robert Jameson. While the flower’s scientific name, Gerbera jamesonii, recollects the name of its founder, the meaning of its common name draws from German naturalist Traugott Gerber. Breeding programs that began in England in 1890 enhanced the flower’s quality and color variations.";
            case button_pom:
                return "Green Button Poms are often known as \"Kermit poms\" or \"Yoko Ono poms.\" Wholesale Button Pom flowers have rounded heads in a spray of about 3 to 5 primary blooms.  Stems are 20-24 inches long, and usually have a number of smaller, less mature secondary blooms. Primary blooms are about 1.5\" in diameter and secondary blooms are about 5/8\" in diameter.";
            case carnation:
                return "The carnation’s history dates back to ancient Greek and Roman times, when it was used in art. Christians believe that the first carnation bloomed on earth when Mary wept for Jesus as he carried his cross. Carnations in these early times were predominantly found in shades of pale pink and peach, but over the years the palette of available colors has grown to include red, yellow, white, purple, and even green. Throughout so many centuries of change, the popularity of the carnation has remained undiminished. The fact that the carnation continues to endure is a testament to its vast appeal.";
            case malaysian_mums:
                return "Mums are a hardy plant that can grow in many different conditions. Many varieties are cultivated to be resistant to insects and drought. Some are also able to resist early frosts and can survive cool temps.\n" +
                        "\n" +
                        "Full direct sunlight is often recommended for good growth and blooms. It is possible to get good results from partial shade or full shade. Full sun may require more water if the weather becomes too hot.\n" +
                        "\n" +
                        "Water is an important factor for encouraging growth and flowers. Soil should be kept slightly moist and not be allowed to fully dry. Over watering should also be avoided as it can lead to stem decay.\n" +
                        "\n" +
                        "For winter hardy plants, continue to water until winter has arrived. For winter, plants should be cut back to just above the soil level. This will prepare them for winter and for the next growing season.";
            case rose:
                return "A rose is a woody perennial flowering plant of the genus Rosa, in the family Rosaceae. There are over 100 species and thousands of cultivars. They form a group of plants that can be erect shrubs, climbing or trailing with stems that are often armed with sharp prickles.";
            case aster:
                return "Aster";
            default:
                return "";
        }
    }

    public
    @DrawableRes
    int getFlowerImage1() {
        switch (this) {
            case crysanthemum:
                return R.drawable.crysanthemum_1;
            case gerbera:
                return R.drawable.gerbera_1;
            case button_pom:
                return R.drawable.buttong_pom_1;
            case malaysian_mums:
                return R.drawable.pink_malaysian_mums;
            case carnation:
                return R.drawable.red_carnation_1;
            case rose:
                return R.drawable.red_rose_1;
            case aster:
                return R.drawable.violet_aster_1;
            default:
                return -1;
        }
    }

    public
    @DrawableRes
    int getFlowerImage2() {
        switch (this) {
            case crysanthemum:
                return R.drawable.crysanthemum_2;
            case gerbera:
                return R.drawable.gerbera_2;
            case button_pom:
                return R.drawable.buttong_pom_2;
            case malaysian_mums:
                return R.drawable.red_malaysian_mums;
            case carnation:
                return R.drawable.red_carnation_2;
            case rose:
                return R.drawable.red_rose_2;
            case aster:
                return R.drawable.violet_aster_2;
            default:
                return -1;
        }
    }

    public
    @DrawableRes
    int getFlowerImage3() {
        switch (this) {
            case crysanthemum:
                return R.drawable.crysanthemum_3;
            case gerbera:
                return R.drawable.gerbera_3;
            case button_pom:
                return R.drawable.buttong_pom_3;
            case malaysian_mums:
                return R.drawable.whitepink_malaysian_mums;
            case carnation:
                return R.drawable.white_carnation_3;
            case rose:
                return R.drawable.red_rose_3;
            case aster:
                return R.drawable.yellow_aster_3;
            default:
                return -1;
        }
    }

    public
    @DrawableRes
    int getFlowerImage4() {
        switch (this) {
            case crysanthemum:
                return R.drawable.crysanthemum_4;
            case gerbera:
                return R.drawable.gerbera_4;
            case button_pom:
                return R.drawable.buttong_pom_4;
            case malaysian_mums:
                return R.drawable.yellow_malaysian_mums;
            case carnation:
                return R.drawable.white_carnation_4;
            case rose:
                return R.drawable.red_rose_4;
            case aster:
                return R.drawable.yellow_aster_4;
            default:
                return -1;
        }
    }

    public Flower[] similarFlowers() {
        switch (this) {
            case crysanthemum:
                return new Flower[]{Flower.gerbera, Flower.malaysian_mums};
            case gerbera:
                return new Flower[]{Flower.crysanthemum, Flower.malaysian_mums};
            case button_pom:
                return new Flower[]{};
            case malaysian_mums:
                return new Flower[]{Flower.crysanthemum, Flower.gerbera};
            case carnation:
                return new Flower[]{};
            case rose:
                return new Flower[]{};
            case aster:
                return new Flower[]{};
            default:
                return new Flower[]{};
        }
    }

    public int[] getUniquFlowerImageRes() {
        switch (this) {
            case crysanthemum:
                return new int[]{R.drawable.crysanthemum_1};
            case gerbera:
                return new int[]{R.drawable.gerbera_1};
            case button_pom:
                return new int[]{R.drawable.buttong_pom_1};
            case malaysian_mums:
                return new int[]{R.drawable.pink_malaysian_mums, R.drawable.red_malaysian_mums,
                        R.drawable.whitepink_malaysian_mums, R.drawable.yellow_malaysian_mums};
            case carnation:
                return new int[]{R.drawable.red_carnation_1, R.drawable.white_carnation_3};
            case rose:
                return new int[]{R.drawable.red_rose_1};
            case aster:
                return new int[]{R.drawable.violet_aster_1, R.drawable.yellow_aster_3};
            default:
                return new int[]{};
        }
    }

}
