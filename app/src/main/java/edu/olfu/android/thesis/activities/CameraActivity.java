package edu.olfu.android.thesis.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.core.Rect;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.ml.CvSVM;

import butterknife.Bind;
import edu.olfu.android.thesis.R;

/**
 * Created by johneris on 2/3/16.
 */
public class CameraActivity extends BaseActivity
        implements CameraBridgeViewBase.CvCameraViewListener2 {

    private static final String TAG = "CameraActivity";

    public static Intent newIntet(Context context) {
        Intent intent = new Intent(context, CameraActivity.class);
        return intent;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_camera;
    }

    @Bind(R.id.cameraView)
    CameraBridgeViewBase mCameraView;

    private CvSVM mCvSVM;
    private Mat mMat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initUI();
    }

    private void initUI() {
        final Window window = getWindow();
        window.addFlags(
                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        mCameraView.setVisibility(SurfaceView.VISIBLE);
        mCameraView.setCvCameraViewListener(this);
        mCameraView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mMat == null) {
                    return;
                }

                int targetCols = 240;
                int targetRows = 240;

                int currentCols = mMat.cols();
                int currentRows = mMat.rows();

                // crop mat

                int toCropWidth = currentRows;
                int toCropHeight = currentRows;
                int toCropX = (currentCols - toCropWidth) / 2;
                int toCropY = 0;

                Rect roi = new Rect(toCropX, toCropY, toCropWidth, toCropHeight);
                Mat croppedMat = new Mat(mMat, roi);

                // resize mat

                Mat resizedMat = new Mat();
                Imgproc.resize(croppedMat, resizedMat, new Size(targetCols, targetRows));

                Log.d(TAG, "MAT cols:" + resizedMat.cols() + " rows:" + resizedMat.rows());

                Bitmap bitmap = Bitmap.createBitmap(mMat.width(), mMat.height(), Bitmap.Config.ARGB_8888);
                Utils.matToBitmap(mMat, bitmap);

                RecognitionActivity.setMat(resizedMat);
                RecognitionActivity.setBitmapResult(bitmap);
                Intent intent = RecognitionActivity.newIntent(mContext);
                startActivity(intent);
            }
        });
    }

    private void onOpenCVLoaded() {
        initSVM();
    }

    private void initSVM() {
        mCvSVM = new CvSVM();
    }

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS: {
                    Log.i(TAG, "OpenCV loaded successfully");
                    mCameraView.enableView();
                    onOpenCVLoaded();
                }
                break;
                default: {
                    super.onManagerConnected(status);
                }
                break;
            }
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_11, this, mLoaderCallback);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mCameraView != null)
            mCameraView.disableView();
    }

    public void onDestroy() {
        super.onDestroy();
        if (mCameraView != null)
            mCameraView.disableView();
    }

    public void onCameraViewStarted(int width, int height) {
    }

    public void onCameraViewStopped() {
    }

    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
//        Mat mat = inputFrame.gray();
//
//        int width = mat.width();
//        int height = mat.height();
//
//        int rectangleWidth = 240;
//        int rectangleHeight = 240;
//
//        int x = (width - rectangleWidth) / 2;
//        int y = (height - rectangleHeight) / 2;
//
//        Rect rect = new Rect(x, y, rectangleWidth, rectangleHeight);
//        Mat cropeed = new Mat(mat, rect);
//
//        Imgproc.rectangle(mat, new Point(rect.x,rect.y), new Point(rect.x+rect.width,rect.y+rect.height),
//                    new Scalar(255, 0, 0, 255), 3);
//
//        Mat mIntermediateMat = new Mat();
//
//        Imgproc.Canny(mat, mIntermediateMat, 80, 90);
//        Imgproc.cvtColor(mIntermediateMat, mat, Imgproc.COLOR_GRAY2BGRA, 4);
//
//        return mat;
        mMat = inputFrame.rgba();
        return inputFrame.rgba();
    }

}
