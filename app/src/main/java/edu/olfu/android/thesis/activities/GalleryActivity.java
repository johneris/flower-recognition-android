package edu.olfu.android.thesis.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import java.util.Arrays;
import java.util.List;

import butterknife.Bind;
import edu.olfu.android.thesis.R;
import edu.olfu.android.thesis.adapters.RVFlowerAdapter;
import edu.olfu.android.thesis.constants.Flower;
import edu.olfu.android.thesis.utils.DividerItemDecoration;

/**
 * Created by johneris on 2/2/16.
 */
public class GalleryActivity extends BaseActivity {

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, GalleryActivity.class);
        return intent;
    }

    @Bind(R.id.rvFlowers)
    RecyclerView mRvFlowers;

    RVFlowerAdapter mRVFlowerAdapter;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_gallery;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initUI();
    }

    private void initUI() {
        setUpToolbar();
        setUpRecyclerView(mRvFlowers);
    }

    private void setUpToolbar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle("Gallery");
            actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    private void setUpRecyclerView(RecyclerView recyclerView) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(
                new DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL_LIST)
        );

        List<Flower> flowers = Arrays.asList(Flower.values());
        // remove negative and aster
        flowers = flowers.subList(0, flowers.size()-2);

        mRVFlowerAdapter = new RVFlowerAdapter(mContext, flowers);
        recyclerView.setAdapter(mRVFlowerAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return false;
    }

}
