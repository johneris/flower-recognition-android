package edu.olfu.android.thesis.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import edu.olfu.android.thesis.R;
import edu.olfu.android.thesis.activities.FlowerDetailsActivity;
import edu.olfu.android.thesis.constants.Flower;

/**
 * Created by johneris on 6/8/2015.
 */
public class RVFlowerAdapter extends RecyclerView.Adapter<RVFlowerAdapter.FlowerViewHolder> {

    List<Flower> mFlowers;
    private Context mContext;

    public RVFlowerAdapter(Context context, List<Flower> flowers) {
        mContext = context;
        mFlowers = flowers;
    }

    @Override
    public FlowerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_flower, parent, false);
        FlowerViewHolder flowerViewHolder = new FlowerViewHolder(view);
        return flowerViewHolder;
    }

    @Override
    public void onBindViewHolder(FlowerViewHolder holder, int position) {
        final Flower flower = mFlowers.get(position);
        holder.ivFlower.setImageResource(flower.getFlowerImage1());
        holder.tvFlower.setText(flower.getName());
        holder.tvDescription.setText(flower.getDescription());
        holder.mainContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = FlowerDetailsActivity.newIntent(mContext, flower.ordinal());
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mFlowers.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void changeData(List<Flower> flowers) {
        mFlowers = flowers;
        notifyDataSetChanged();
    }

    public static class FlowerViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.mainContainer)
        View mainContainer;

        @Bind(R.id.ivFlower)
        ImageView ivFlower;

        @Bind(R.id.tvFlower)
        TextView tvFlower;

        @Bind(R.id.tvDescription)
        TextView tvDescription;

        FlowerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
